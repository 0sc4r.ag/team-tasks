<!--

Set issue title as "Scheduling issue for the MAJOR.MINOR release" where MAJOR.MINOR is the GitLab version.

This template is used to create a release's scheduling issue. The scheduling issue
is used to raise visibility to the team on what issues are being looked at as
candidates for scheduling, and provides a place for them to offer up scheduling suggestions.

Engineering manager and Product manager are responsible for scheduling
work that is a direct responsibility of the Distribution team.

Typically the Engineering manager is the one to create this issue as part of their scheduling workflow

-->

### Current OKRs

<!--
List the current quarter's Distribution Team OKRs
-->

1. ...
2. ...
3. ...


### Team Availability

<!--

Use the Distribution time-off calendar, and the the team meeting agenda doc to determine
what availability the team will have for the release.

- 1 backend engineer at 50%
- 2 backend engineers at 100%

Total 250% of backend engineer availability

-->

Total % of backend engineer availability

### Priority and Severity boards

Priority Boards categorize the importance of all open issues, regardless of milestone. They are useful when looking for
issue to consider for scheduling.

- [Omnibus Priority board](https://gitlab.com/gitlab-org/omnibus-gitlab/boards/596881)
- [Chart Priority board](https://gitlab.com/charts/gitlab/boards/817067)
- [Team tasks Priority board](https://gitlab.com/gitlab-org/distribution/team-tasks/boards/1078569)

### Scheduling boards

<!--

The Scheduling boards can be used to organize the issue candidates. The boards should be curated/scoped down by the Engineering Manager
and Product Manager during their regular meetings. Once the lists are agreed upon, the issues will get the milestone assigned

-->  

Scheduling boards are a work board used specifically during the scheduling process to give an overview of the candidates, and
used to get a idea of the next release before assigning the milestone to the issues.

- [Omnibus In Scheduling board](https://gitlab.com/gitlab-org/omnibus-gitlab/boards/1078572?)
- [Charts In Scheduling board](https://gitlab.com/charts/gitlab/boards/1078581)
- [Team tasks In Scheduling board](https://gitlab.com/gitlab-org/distribution/team-tasks/boards/1078579)

### Status

1. [ ] Collect initial candidate issues
1. [ ] Issues added to the scheduling boards
1. [ ] Scheduling boards sorted/arranged by importance
1. [ ] Scheduling boards contain candidates from both Engineering and Product
1. [ ] Scheduling boards scoped to a reasonable amount of work for the release
1. [ ] Managers schedule the agreed issues for the release
